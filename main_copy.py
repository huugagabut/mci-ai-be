from distutils.log import debug
import json
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import os
# from google.cloud import translate
from googletrans import Translator

translator = Translator()
languange_code_dest = ["ms", "zh-cn", "ta" ]
# output = translator.translate('Good morning, guys', src='en', dest='id')
# output = translator.translate('Mikä on nimesi', src='fi', dest='fr')
# print(output.text)

def translate_result(result):
  json_result = {}
  for lang in languange_code_dest:
    output = translator.translate(result, src='en', dest=lang)
    json_result[lang] = output.text
    # print("output ", output.text)
  print("json result ", json_result)
  return json_result

def translate(result, lang):
  if (lang == ''):
    return result
  output = translator.translate(result, src='en', dest=lang)
  return output.text

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# setup Happy Transformer - AI

# happy_gen = HappyGeneration(load_path="model/model-125M-178")
# top_k_sampling_settings = GENSettings(do_sample=True, early_stopping=False, top_k=50, temperature=0.7)

@app.route("/")
def home():
  return "output"

@app.route("/generate", methods=['POST'])
@cross_origin()
def generate():
  keyword = request.form['keyword']
  language = request.form['language']
  result = {"text": keyword}
  # print(translate_result(result["text"]))
  return jsonify({'keyword' : keyword, 'result' : translate(result["text"], language) }) 
  # return jsonify({'keyword' : keyword, 'result' : result["text"], 'translated_result': translate_result(result["text"])})


if __name__ == "__main__":
  app.run(debug=True)