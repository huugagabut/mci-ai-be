from distutils.log import debug
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from happytransformer import HappyGeneration, GENSettings

from googletrans import Translator

import torch
from transformers import T5ForConditionalGeneration,T5Tokenizer

import re
# as per recommendation from @freylis, compile once only
CLEANR = re.compile('<.*?>') 

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = T5ForConditionalGeneration.from_pretrained("Michau/t5-base-en-generate-headline")
tokenizer = T5Tokenizer.from_pretrained("Michau/t5-base-en-generate-headline")
model = model.to(device)


translator = Translator()
languange_code_dest = ["ms", "zh-cn", "ta" ]

def translate(result, current_language,  lang):
  if (lang == ''):
    return result
  output = translator.translate(result, src=current_language, dest=lang)
  print('out',output)
  return output.text

def generate_title(result):
  max_len = 256
  text =  "headline: " + result

  encoding = tokenizer.encode_plus(text, return_tensors = "pt")
  input_ids = encoding["input_ids"].to(device)
  attention_masks = encoding["attention_mask"].to(device)

  beam_outputs = model.generate(
      input_ids = input_ids,
      attention_mask = attention_masks,
      max_length = 64,
      num_beams = 3,
      early_stopping = True,
  )

  result = tokenizer.decode(beam_outputs[0])
  
  return cleanhtml(result)

def cleanhtml(raw_html):
  cleantext = re.sub(CLEANR, '', raw_html)
  return cleantext


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# setup Happy Transformer - AI

happy_gen = HappyGeneration(load_path="/home/hugo_irwanto/model/model-125M-178")

@app.route("/")
def home():
  return " "

@app.route("/generate", methods=['POST'])
@cross_origin()
def generate():
  keyword = request.form['keyword']
  language = request.form['language']
  words = int(request.form['words'])

  print("keyword", keyword)
  print('lang', language)
  top_k_sampling_settings = GENSettings(do_sample=True, early_stopping=False, top_k=50, temperature=0.7, min_length=int(words * 0.95),  max_length=words, no_repeat_ngram_size=2)
  result = happy_gen.generate_text(keyword, args=top_k_sampling_settings)
  translated_text = translate(result.text, 'en', language)

  title = generate_title(result.text)
  translated_title = translate(title, 'en', language)
  return jsonify({'keyword' : translated_title, 'result' : translated_text }) 
  # return jsonify({'keyword' : keyword, 'result' : result.text})

@app.route("/translate", methods=['POST'])
@cross_origin()
def translate_func():

  # if (request.form['title'] != None):
  title = request.form.get('title')
  print('title -translate', title)
  print ('title type ', type(title))

  text = request.form['text']
  language = request.form['language']
  current_language = request.form['current_language']
  print("------translated------")
  print("text", text)
  print('lang', language)

  translated_text = translate(text, current_language, language)

  if (title == "" or title == None):
    return jsonify({'translated_text' : translated_text})
  else:
    translated_title = translate(title, current_language, language)

  return jsonify({'translated_title' : translated_title, 'translated_text' : translated_text }) 


if __name__ == "__main__":
  app.run(host='0.0.0.0', port=5000, debug=True)
